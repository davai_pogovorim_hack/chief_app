from yoyo import step

step(
    """
    CREATE TABLE `user` (
    `id` INT unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `user_id` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `platform` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `status_theme` INT NOT NULL DEFAULT '0',
    `status_form` INT NOT NULL DEFAULT '0',
    `answers_form` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' NOT NULL
    );
    """
)
