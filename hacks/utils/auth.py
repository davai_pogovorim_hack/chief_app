import functools
import json

from base64 import b64encode
from collections import OrderedDict
from hashlib import sha256
from hmac import HMAC
from urllib.parse import urlparse, parse_qsl, urlencode

from hacks.models.users import User
from hacks.settings.conf import *
from .errors import ForbiddenError
