import uuid
import typing as t
from asyncio import TimeoutError
from json.decoder import JSONDecodeError

import aiohttp
from aiohttp.client_exceptions import ClientConnectorError
from hacks.settings import conf as settings


class JsonRpcClientError(Exception):
    status: t.Optional[int]
    message: str
    data: t.Dict[str, t.Any]

    def __init__(self, status=None, message='', data=None):
        self.status = status
        self.message = message
        if data is None:
            data = dict()
        self.data = data
        super().__init__(f'{self.status}, message={self.message}')


class JsonRpcClient:
    """Базовый JsonRPC клиент."""
    #: адрес сервера
    _url: str
    _session: aiohttp.ClientSession
    _timeout: aiohttp.ClientTimeout

    def __init__(self, _url='', _timeout=120):
        self._url = _url
        self._timeout = _timeout

    @staticmethod
    def _get_id():
        return uuid.uuid4().int & (1 << 32) - 1

    async def request(self, method, params, timeout=None, headers={}):
        data = {
            'id': self._get_id(),
            'jsonrpc': '2.0',
            'method': method,
            'params': params,
        }
        if timeout is None:
            timeout = self._timeout
        async with self._session.post(
            url=self._url,
            json=data,
            timeout=int(timeout),
            headers=headers,
        ) as response:
            try:
                resp = await response.json(content_type=None)
            except JSONDecodeError:
                print("Ошибка на сервисе", response.text())
            if 'error' in resp:
                if isinstance(resp['error'], dict):
                    raise JsonRpcClientError(
                        status=resp['error'].get('code'),
                        message=resp['error'].get('message', ''),
                        data=resp['error'].get('data'),
                    )
                else:
                    raise JsonRpcClientError(
                        message=str(resp['error'])
                    )
            return resp['result']

    async def request_safe(self, *args, **kwargs):
        try:
            return await self.request(*args, **kwargs)
        except (
            JsonRpcClientError,
            ClientConnectorError,
            TimeoutError,
        ) as e:
            print(e)
            return None

    async def close(self):
        await self._session.close()


class BaseClient(JsonRpcClient):
    """Основной класс клиента."""

    def __init__(self, verify_ssl=False):
        super().__init__(self._url)
        self._session = aiohttp.ClientSession(
            connector=aiohttp.TCPConnector(verify_ssl=verify_ssl),
            raise_for_status=True,
        )

    async def send(
        self,
        method: str,
        params: dict,
        timeout: int = None,
        headers: dict = {},
    ):
        return await self.request_safe(method, params, timeout, headers)


class ConsumerApi(BaseClient):
    """Клиент кампаний"""
    _url = settings.API_CONSUMER_APP


class Client:
    _consumer: ConsumerApi

    def __init__(self):
        self._consumer = ConsumerApi()

    @property
    def consumer(self):
        return self._consumer


client = Client()
