from datetime import datetime

from hacks.models.base import BaseModel
import peewee


class User(BaseModel):
    user_id = peewee.CharField(
        verbose_name="user_id",
        max_length=255
    )
    platform = peewee.CharField(
        verbose_name="platform",
        max_length=255,
    )
    status_theme = peewee.IntegerField(
        verbose_name="status_theme",
        default=0,
    )
    status_form = peewee.IntegerField(
        verbose_name="status_form",
        default=0,
    )
    answers_form = peewee.CharField(
        verbose_name="status_form",
        max_length=255,
        default="",
    )

    @classmethod
    async def get_or_create_by_vk_id(cls, vk_id):
        user = cls.select().where(
            cls.vk_id == vk_id,
        ).first()
        if user is None:
            user = await cls.create_object(
                vk_id=vk_id,
                time_registered=datetime.now(),
                last_login=datetime.now(),
            )
            return user
        else:
            await user.update_object(
                query=dict(last_login=datetime.now())
            )
            return user

    async def get_full_info(self):
        return dict(
            snow=self.snow,
        )
