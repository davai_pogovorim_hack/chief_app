import datetime
import typing as t

import peewee

from hacks.settings.conf import (
    database,
    db_manager,
)


class BaseModel(peewee.Model):
    class Meta:
        database = database

    def save(self, *args, **kwargs):
        """При сохранении объекта, метод обновляет поле updated_at"""
        self.updated_at = datetime.datetime.now()
        return super().save(*args, **kwargs)

    @classmethod
    async def create_object(cls, **query):
        """Создание объекта в базе"""
        return await db_manager.create(cls, **query)

    @classmethod
    async def get_or_create_object(cls, **query):
        """Создать или получить, если он уже создан объект"""
        return await db_manager.get_or_create(cls, **query)

    @classmethod
    async def get_object(cls, id):
        """Получение объекта по id"""
        try:
            return await db_manager.get(cls, id=id)
        except:
            return None

    @classmethod
    async def get_objects(
            cls,
            ids: t.List[int],
            fields: t.Optional[list] = None,
    ):
        """

        Получение нескольких объектов

        :param ids: список идентификаторов
        :param fields: список полей модели, по которым делаем select()

        """

        if fields:
            query = cls.select(*fields).where(cls.id << ids)
        else:
            query = cls.select().where(cls.id << ids)

        objects = await db_manager.execute(query)
        return objects

    @classmethod
    async def execute(cls, this):
        return await db_manager.execute(this)

    async def update_object(self, query):
        """Обновление данных объекта"""
        for key, value in query.items():
            setattr(self, key, value)
        await db_manager.update(self)

    async def delete_object(self):
        """Удаление объекта"""
        await db_manager.delete(self)

    @staticmethod
    async def bulk_delete(objects):
        """Удаление нескольких объектов"""
        async with db_manager.atomic():
            for obj in objects:
                await obj.delete_object()

    @classmethod
    async def paginate(
            cls,
            objects,
            page: t.Optional[int] = None,
            count: t.Optional[int] = None,
    ):
        if (
            page is None or
            count is None
        ):
            return objects

        objects_new = objects.paginate(page, count)
        return objects_new
