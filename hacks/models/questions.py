from random import choice
from datetime import datetime
import peewee
from hacks.models.base import BaseModel
from hacks.models.users import User
from hacks.settings.conf import db_manager


class Themes(BaseModel):
    name = peewee.CharField(
        verbose_name="theme_name",
    )

    @classmethod
    async def get_all_themes(cls):
        rows = await db_manager.execute(
            cls.select().order_by(cls.id.desc())
        )
        return [
            {
                "id": item.id,
                "name": item.name,
            } for item in rows
        ]


class FirstQuestion(BaseModel):
    question = peewee.TextField(
        verbose_name="question"
    )
    theme = peewee.ForeignKeyField(
        Themes,
        on_delete="CASCADE"
    )

    @classmethod
    async def get_questions_by_theme_id(cls, theme_id):
        resps = await db_manager.execute(
            cls.select().join(
                Themes
            ).where(
                cls.theme == theme_id,
            ).order_by(cls.id.desc())
        )
        return [
            {
                "id": resp.id,
                "question": resp.question,
                "theme": {
                    "id": resp.theme.id,
                    "name": resp.theme.name,
                }
            } for resp in resps
        ]


class PostcardGreeting(BaseModel):
    text = peewee.TextField(
        verbose_name="text",
    )

    @classmethod
    async def get_random_greeting(cls):
        rows = await cls.execute(
            cls.select(
                cls.id,
                cls.text
            ).order_by(peewee.fn.Rand()).limit(5)
        )
        return [
            dict(id=x.id, text=x.text)
            for x in rows
        ]


class PostcardDesign(BaseModel):
    path = peewee.CharField(
        verbose_name="path"
    )

    async def get_full_info(self):
        return dict(
            id=self.get_id(),
            url=self.path,
        )


class PostcardData(BaseModel):
    from_user = peewee.ForeignKeyField(
        User,
        on_delete="CASCADE"
    )
    to_user = peewee.ForeignKeyField(
        User,
        on_delete="CASCADE"
    )
    anonymous = peewee.BooleanField(
        verbose_name="anonymous",
        default=False,
        null=False
    )
    text = peewee.TextField(
        verbose_name="text",
    )
    slug = peewee.CharField(
        verbose_name="slug",
        max_length=255,
    )
    design = peewee.ForeignKeyField(
        PostcardDesign,
        on_delete="CASCADE",
    )
    mark_id = peewee.SmallIntegerField(
        default=0,
        verbose_name="mark_id",
    )
    time_send = peewee.DateTimeField(
        verbose_name="last_login",
    )
    is_view = peewee.BooleanField(
        verbose_name="is_view",
        default=False,
    )

    async def get_short_info(self):
        from_user_id = None if self.anonymous else self.from_user.vk_id
        return dict(
            id=self.id,
            from_user_id=from_user_id,
            slug=self.slug,
            is_view=self.is_view,
            time_send=self.time_send,
        )

    async def get_full_info(self):
        from_user_id = None if self.anonymous else self.from_user.vk_id
        return dict(
            from_user_id=from_user_id,
            text=self.text,
            design=await self.design.get_full_info(),
            slug=self.slug,
            is_view=self.is_view,
            mark_id=self.mark_id,
        )

    @classmethod
    async def create_and_generate_slug(cls, **kwargs):
        sumbols = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        slug = ''.join(choice(sumbols) for i in range(15))
        kwargs['slug'] = slug
        return await cls.create_object(
            **kwargs
        )

    @classmethod
    async def get_no_view_count_postcard_by_user(cls, user) -> dict:
        new_count = cls.select().where(
            cls.to_user == user,
            cls.is_view == False
        ).order_by(cls.id.desc()).count()
        return new_count

    @classmethod
    async def get_all_by_x_user_id(cls, x_user_id):
        user = await User.get_object(x_user_id)
        array = cls.select().where(
            cls.to_user == user,
        ).order_by(cls.id.desc())

        return [await x.get_short_info() for x in array]

    @classmethod
    async def get_or_none_by_slug_and_to_user_id(cls, x_user_id, slug) -> tuple:
        """
        Получение карточки по параметрам
        :param x_user_id:
        :param slug:
        :return:
            0 -> is_error
            1 -> info
        """
        user = await User.get_object(x_user_id)
        card = cls.select().where(
            cls.to_user == user,
            cls.slug == slug,
        ).order_by(cls.id.desc()).first()
        if card is None:
            user = await User.get_or_create_by_vk_id(0)
            card = cls.select().where(
                cls.to_user == user,
                cls.slug == slug,
            ).order_by(cls.id.desc()).first()
            if card is None:
                return True, dict(details="Slug is incorrect or access denied")
        await card.update_object(
            dict(is_view=True)
        )
        return False, await card.get_full_info()
