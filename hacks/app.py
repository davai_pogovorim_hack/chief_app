import fastapi_jsonrpc as jsonrpc
# Создаём приложение fast api и точку входа

app = jsonrpc.API()

api_v1 = jsonrpc.Entrypoint(
    "/jsonrpc",
    errors=jsonrpc.Entrypoint.default_errors,
)

if __name__ == "__main__":
    def migration():
        print("Start migration in command:")
        from hacks.settings import conf
        DB_USER = conf.DB_USER
        DB_PASSWORD = conf.DB_PASSWORD
        DB_HOST = conf.DB_HOST
        DB_NAME = conf.DB_NAME
        migrate = f"yoyo apply --database mysql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME} hacks/migrations"
        print(migrate)

    import uvicorn
    migration()
    uvicorn.run('hacks:app', host="0.0.0.0", port=5000, debug=True)
