from pydantic import BaseModel
import typing as t


class ResponseAppKeyboardsItem(BaseModel):
    text: str
    color: str = "primary"


class ResponseAppKeyboardsRow(BaseModel):
    buttons: t.List[ResponseAppKeyboardsItem]


class ResponseApp(BaseModel):
    text: str
    user_id: str
    keyboards: t.List[ResponseAppKeyboardsRow] = None


class RequestApp(BaseModel):
    platform: str
    user_id: str
    body: str


def get_keyboard_yes_and_no():
    return [
        ResponseAppKeyboardsRow(
            buttons=[ResponseAppKeyboardsItem(text="Да"), ResponseAppKeyboardsItem(text="Нет")]
        )
    ]


def get_keyboard_consultation():
    return [
        ResponseAppKeyboardsRow(
            buttons=[ResponseAppKeyboardsItem(text="Получить консультацию")]
        )
    ]
