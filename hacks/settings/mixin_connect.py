from peewee import (InterfaceError, SENTINEL, OperationalError)
from playhouse.shortcuts import ReconnectMixin
from peewee_async import (Manager, PooledMySQLDatabase)
import functools


class ReconnectError(Exception):
    pass


class MyReconnectMixin(ReconnectMixin):
    reconnect_errors = (
        (InterfaceError, 'connection already closed'),
        (InterfaceError, "(0, '')"),
        (OperationalError, 'lost connection to mysql server during query'),
        (OperationalError, 'server has gone away'),
    )

    def execute_sql(self, sql, params=None, commit=SENTINEL):
        try:
            return super(ReconnectMixin, self).execute_sql(sql, params, commit)
        except Exception as exc:
            exc_class = type(exc)
            if exc_class not in self._reconnect_errors:
                raise exc

            exc_repr = str(exc).lower()
            for err_fragment in self._reconnect_errors[exc_class]:
                if err_fragment in exc_repr:
                    break
            else:
                raise exc

            if not self.is_closed():
                print('Reconnect to db...')
                # Удаляем все транзакции, с ними не получится закрыть соединение
                if self.in_transaction():
                    for i in range(self.transaction_depth()):
                        self.pop_transaction()
                self.close()
                self.connect()
                print('Reconnection success')
            # Исключение нужно обработать ниже, и при необходимоти повторить всю транзакцию заново
            raise ReconnectError


class MQDataBase(MyReconnectMixin, PooledMySQLDatabase):
    def init(self, database, min_connections=1, max_connections=10, **connect_kwargs):
        super().init(database, min_connections=min_connections, max_connections=max_connections, **connect_kwargs)


def retry_connect(max_tries=5):
    def decorator(func):
        @functools.wraps(func)
        async def wrapper(*args, **kwargs):
            for n in range(max_tries):
                try:
                    return await func(*args, **kwargs)
                except ReconnectError:
                    if n == max_tries - 1:
                        raise
        return wrapper
    return decorator
