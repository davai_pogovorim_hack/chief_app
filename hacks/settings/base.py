import os

ALLOW_URL_CORS = [
    "https://localhost:10888",
    "http://localhost:8080",
]
AMQP_URL = os.getenv("AMQP_URL")

DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_PASSWORD = os.getenv('DB_PASSWORD')

SITE_DOMAIN = os.getenv('SITE_DOMAIN')
RUN_TYPE = os.getenv('RUN_TYPE')
SECRET_PATH = os.getenv('SECRET_PATH')
GOOGLE_SHEET_NAME = os.getenv("GOOGLE_SHEET_NAME")
JSON_PATH_ONE = os.getenv('JSON_PATH_ONE')
JSON_PATH_TWO = os.getenv('JSON_PATH_TWO')
API_CONSUMER_APP = os.getenv("API_CONSUMER_APP")

