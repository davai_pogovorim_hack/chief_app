import datetime
import typing as t
from fastapi import BackgroundTasks

from hacks.logics.firstquestions_normalaize import logic_normaliaze_string
from hacks.utils.auth import retry_connect
from hacks.app import api_v1
from hacks.models.questions import Themes, FirstQuestion


@api_v1.method()
@retry_connect()
async def questions_modeles(
        theme: str,
) -> dict:
    pass


@api_v1.method()
@retry_connect()
async def themes_create(
        theme: str,
) -> dict:
    theme = await Themes.create_object(theme=theme)
    return dict(
        id=theme.id,
        name=theme.name,
    )


@api_v1.method()
@retry_connect()
async def themes_update_by_id(
        theme_id: int,
        name: str,
) -> dict:
    theme = await Themes.get_object(theme_id)
    await theme.update_object(name=name)
    return dict(
        id=theme.id,
        name=theme.name,
    )


@api_v1.method()
@retry_connect()
async def themes_delete_by_id(
        theme_id: int,
) -> dict:
    theme = await Themes.get_object(theme_id)
    await theme.delete_object()
    return dict(
        status="ok"
    )


@api_v1.method()
@retry_connect()
async def themes_question_create(
        theme_id: int,
        questions: t.List[str],
) -> list:
    theme = await Themes.get_object(theme_id)
    question_objects = []
    for question in questions:
        try:
            question = logic_normaliaze_string(question)
        except:
            continue
        question_objects.append(
            await FirstQuestion.create_object(
                question=question,
                theme=theme
            )
        )
    return [
        {
            "id": item.id,
            "question": item.question
        } for item in question_objects
    ]


@api_v1.method()
@retry_connect()
async def themes_question_update(
        question_id: int,
        question_name: str,
) -> dict:
    question = await FirstQuestion.get_by_id(question_id)
    await question.update_object(
        question_name=logic_normaliaze_string(question_name)
    )
    return {
            "id": question.id,
            "question": question.question
        }


@api_v1.method()
@retry_connect()
async def themes_question_remove(
        question_id: int,
) -> dict:
    question = await FirstQuestion.get_by_id(question_id)
    await question.delete_object()
    return {
        "status": "ok"
    }