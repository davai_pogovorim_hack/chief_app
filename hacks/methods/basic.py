import asyncio
from fastapi_jsonrpc import BackgroundTasks
from hacks.app import api_v1
from hacks.logics.google_sheets import ThemesQuestion, get_theme_id_by_question, reloaded_google_sheets__logic
from hacks.models.users import User
from hacks.utils.async_client import client
from hacks.validates.service_ans import get_keyboard_yes_and_no, ResponseApp, get_keyboard_consultation


@api_v1.method()
async def echo() -> dict:
    return dict(
        status="ok",
        text="api is start"
    )


def init_jsons():
    theme_object = ThemesQuestion()
    theme_object.get_load()
    return theme_object.data, theme_object.questions_json_object


async def logic_states(
        platform: str,
        user_id: str,
        body: str,
) -> ResponseApp:
    user_db, _ = await User.get_or_create_object(
        platform=platform,
        user_id=user_id
    )
    theme_object, questions_object = init_jsons()
    body = body.lower()
    if body == "получить консультацию":
        return ResponseApp(
                text="Для получение конультации перейдите по ссылке http://f0496561.xsph.ru",
                user_id=user_id,
            )
    if body in ["/start", "как дела?", "как настроение?", "как дела", "как настроение", "сколько времени?", "сколько времени"]:
        return ResponseApp(
            text="Опишите вашу проблему с ребенком, мы поймем и дадим совет. \nХватит одного предложения.",
            user_id=user_id,
        )
    if user_db.status_theme not in [int(x.get("id")) for x in theme_object.values()]:
        # Определяем тему проблемы
        result = get_theme_id_by_question(body)
        theme_name_id = int(result.get("theme_id", "0"))
        if theme_name_id > 0:
            user_db.status_theme = theme_name_id
            # Составляем вопросы, фигурируем первый
            questions_on_theme = questions_object.get(str(user_db.status_theme)).get("questions")
            resp = ResponseApp(
                text=questions_on_theme["1"],
                user_id=user_id,
            )
            resp.keyboards = get_keyboard_yes_and_no()
            await user_db.update_object(query=dict(
                status_theme=theme_name_id,
                answers_form="",
                status_form=1,
            ))
            return resp
        else:
            # Говорим что не сталкивались с подобными ситуациями
            resp = ResponseApp(
                text="Мы ещё не добавили такую проблему, но скоро её добавим\nЗапишитесь на консульацию, если это срочно",
                user_id=user_id,
            )
            resp.keyboards = get_keyboard_consultation()
            return resp
    else:
        questions_on_theme = questions_object.get(str(user_db.status_theme)).get("questions")
        answers_on_theme = questions_object.get(str(user_db.status_theme)).get("answers")
        if user_db.status_form in [int(x) for x in questions_on_theme.keys()]:
            if body in ["да", "д", "yes", "y"]:
                user_db.answers_form += "1"
            elif body in ["нет", "н", "no", "n"]:
                user_db.answers_form += "0"
            else:
                # Повторяем вопрос:
                text = questions_on_theme[str(user_db.status_form)]
                resp = ResponseApp(
                    text=text,
                    user_id=user_id,
                )
                resp.keyboards = get_keyboard_yes_and_no()
                return resp

            if len(user_db.answers_form) == len(questions_on_theme):
                # Отправляем результат
                if user_db.answers_form in answers_on_theme:
                    text = answers_on_theme.get(user_db.answers_form)
                else:
                    text = "Это что-то новое, необходимо нам проверить техническую часть!"
                await user_db.update_object(query=dict(
                    answers_form="",
                    status_theme=0,
                    status_form=0,
                ))
                return ResponseApp(
                    text=text,
                    user_id=user_id,
                )
            else:
                # Отправляем следующий вопрос
                await user_db.update_object(query=dict(
                    status_form=user_db.status_form + 1,
                ))
                text = questions_on_theme[str(user_db.status_form)]
                resp = ResponseApp(
                    text=text,
                    user_id=user_id,
                )
                resp.keyboards = get_keyboard_yes_and_no()
                return resp


async def logic(
        platform: str,
        user_id: str,
        body: str,
):
    resp = await logic_states(
        platform=platform,
        user_id=user_id,
        body=body
    )
    resp.user_id = user_id
    proxy_pass = {
        "vk": "send_to_vk",
        "viber": "send_to_viber",
        "telegram": "send_to_telegram"
    }
    retry = 5
    for i in range(retry):
        try:
            await client.consumer.send(
                method=proxy_pass[platform],
                params={
                    "data": resp.dict()
                }
            )
            break
        except Exception as err:
            await asyncio.sleep(5)


@api_v1.method()
async def send_message_platform(
        tasks: BackgroundTasks,
        platform: str,
        user_id: str,
        body: str,
) -> dict:
    if platform not in ["vk", "viber", "telegram"]:
        raise ModuleNotFoundError("Платформа не найдена")
    tasks.add_task(
        func=logic,
        platform=platform,
        user_id=user_id,
        body=body
    )
    return dict(status="ok")


@api_v1.method()
async def reloaded_google_sheets() -> dict:
    reloaded_google_sheets__logic()
    return dict(status="ok")


