import typing as t
from hacks.app import api_v1
from hacks.utils.auth import retry_connect
from hacks.models.users import User
from hacks.settings.conf import db_manager


@api_v1.method()
@retry_connect()
async def user_open_app(
        sign: str,
        x_user_id: int = -1
) -> dict:
    """
    Функция, которая отдаёт первичные данные для входа в приложение
    :return:
    """
    return dict(hui="hui")
