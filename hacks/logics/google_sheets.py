from oauth2client.service_account import ServiceAccountCredentials
import hacks.settings.conf as cnf
import gspread
import json

from hacks.logics.firstquestions_normalaize import logic_normaliaze_string
from hacks.logics.meta import SingletonMeta


class GoogleSheets:
    def __init__(self):
        self.gcredentials = cnf.SECRET_PATH
        self.gdocument = cnf.GOOGLE_SHEET_NAME
        self.gscope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
        # Запись в Google Sheet
        credentials = ServiceAccountCredentials.from_json_keyfile_name(self.gcredentials, self.gscope)
        self.gc = gspread.authorize(credentials)
        self.wks_all = self.gc.open(self.gdocument)
        self.sheets: dict = dict((
            (x.title, x)
            for x in self.wks_all.worksheets()
        ))
        self.theme_objects: dict = dict()
        self.questions_json_object: dict = dict()

    def get_all_themes(self):
        name = "Themes"
        table = self.sheets[name].get()
        for i in range(1, len(table)):
            row = table[i]
            if len(row) == 3 and len(row[1]) != 0 and len(row[2]) != 0 and row[2] in self.sheets:
                self.theme_objects[row[1]] = dict(
                    id=row[0],
                    name=row[1],
                    table_name=row[2],
                    questions=[]
                )

    def get_questions_ans_answers_by_table_name(self, table_name):
        def parse_yes_or_none(item):
            item = item.lower()
            if item in ["да", "д", "yes", "y"]:
                return "1"
            return "0"

        table = self.sheets[table_name].get()
        answers = dict()  # answers 0001010100... -> result
        questions = dict()  # dict(id, question)]
        # get_all_questions ->
        for i in range(1, len(table)):
            row = table[i]
            if len(row) >= 2 and len(row[0]) != 0 and len(row[1]) != 0:
                questions[row[0]] = row[1]
        # get_all_answers ->
        m = len(table[0])
        for j in range(2, m):
            result = ""
            answer = []
            for i in range(0, len(table)):
                if i == 0:
                    result = table[i][j]
                else:
                    answer.append(parse_yes_or_none(table[i][j]))
            answers["".join(answer)] = result
        return {
            "answers": answers,
            "questions": questions,
            "name": table_name,
        }

    def get_first_question(self):
        name = "FirstQurstion"
        table = self.sheets[name].get()
        for i in range(1, len(table)):
            row = table[i]
            print(row[2])
            if len(row) == 3 and len(row[1]) != 0 and len(row[2]) != 0 and row[2] in self.theme_objects:
                if "questions" not in self.theme_objects[row[2]]:
                    self.theme_objects[row[2]]["questions"] = []
                self.theme_objects[row[2]]["questions"].append(row[1])

    def normaliaze(self):
        for key in self.theme_objects.keys():
            value = self.theme_objects[key]
            for i in range(len(value["questions"])):
                self.theme_objects[key]["questions"][i] = logic_normaliaze_string(value["questions"][i])

    def load_questions_on_tables(self):
        questions_json_object = dict()
        for key in self.theme_objects.keys():
            table_name = self.theme_objects[key].get("table_name")
            id = self.theme_objects[key].get("id")
            questions_json_object[id] = self.get_questions_ans_answers_by_table_name(table_name)
            questions_json_object[id].update(theme_id=id)
        return questions_json_object

    def save_to_json(self):
        self.get_all_themes()
        self.get_first_question()
        self.normaliaze()
        with open(cnf.JSON_PATH_ONE, 'w', encoding='utf8') as outfile:
            json.dump(self.theme_objects, outfile, ensure_ascii=False)
        self.questions_json_object = self.load_questions_on_tables()
        with open(cnf.JSON_PATH_TWO, 'w', encoding='utf8') as outfile:
            json.dump(self.questions_json_object, outfile, ensure_ascii=False)
        r = ThemesQuestion()
        r.reload()


class ThemesQuestion(metaclass=SingletonMeta):
    data = None
    questions_json_object = None

    def reload(self):
        try:
            with open(cnf.JSON_PATH_ONE) as json_file:
                self.data = json.load(json_file)

            with open(cnf.JSON_PATH_TWO) as json_file:
                self.questions_json_object = json.load(json_file)
        except:
            pass

    def get_load(self):
        if self.data is None or self.questions_json_object is None:
            self.reload()
        return self.data


def get_theme_id_by_question(question_req: str):
    from fuzzywuzzy import fuzz
    question_req = logic_normaliaze_string(question_req)
    default = dict(question="", theme_name="Не определена", theme_name_id="0", ratio=0)
    result = default
    theme = ThemesQuestion()
    data = theme.get_load()
    for theme_object in data.values():
        theme_name = theme_object.get("name", "")
        theme_id = theme_object.get("id", "0")
        for question in theme_object.get("questions", []):
            ratio = fuzz.token_set_ratio(question_req, question)
            if ratio > result["ratio"]:
                result = dict(
                    question=question,
                    theme_name=theme_name,
                    theme_id=theme_id,
                    ratio=ratio,
                )
    return result if result["ratio"] > 45 else default


def reloaded_google_sheets__logic():
    t = ThemesQuestion()
    t.get_load()
    g = GoogleSheets()
    g.save_to_json()
    t.get_load()