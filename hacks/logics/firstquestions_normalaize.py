import pymorphy2
import re
import string as string_c
from hacks.logics.meta import SingletonMeta


class Singleton(metaclass=SingletonMeta):
    morph = None

    def pymorphy2(self):
        if self.morph is None:
            self.morph = pymorphy2.MorphAnalyzer()

    def clear_string(self, words):
        r = re.compile("[а-яА-Я]+")
        russian = [w for w in filter(r.match, words)]
        return list(filter(self.filter_string, russian))

    def filter_string(self, word):
        proxy = ["мой", "мое", "ребенок", "сын", "дочь", "я", "причина", "мной"]
        for s in proxy:
            if word.startswith(s):
                return False
        return True

    def normalized_word(self, word):
        parses = self.morph.parse(word)
        if len(parses) > 0:
            return parses[0].normal_form.replace("ё", "е")
        return word

    def loop_string(self, string):
        string = re.sub(r'\s+', ' ', string)
        string = string.lower()
        words_list = list(map(str, string.split()))
        words_list = self.clear_string(words_list)
        words_list = [self.normalized_word(word) for word in words_list]
        return " ".join(words_list)


def logic_normaliaze_string(string: str) -> str:
    s = Singleton()
    s.pymorphy2()
    exclude = set(string_c.punctuation)
    string = ''.join(ch for ch in string if ch not in exclude)
    string = string.replace("ё", "е")
    string = string.lower()
    return s.loop_string(string)
