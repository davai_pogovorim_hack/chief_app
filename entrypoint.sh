#!/bin/bash

echo "Apply migrations and basic data creation "
yoyo apply --database mysql://$DB_USER:$DB_PASSWORD@$DB_HOST/$DB_NAME newyear/migrations

# Start server
echo "Starting server"
uvicorn hacks:app --host 0.0.0.0 --port=80
